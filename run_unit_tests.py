import unittest
from tempfile import NamedTemporaryFile
from io import StringIO
import os
from dretoolslib.parsers import *
from dretoolslib.shared import *
from dretoolslib.merge import *

# python3 -m unittest run_unit_tests.py
# python -m unittest tests/test_something.py
# python -m unittest -v test_module

test_path = "dretoolslib/test_data/"

def test_values(expected_value, test_value):
    """

    :param expected_value:
    :param test_value:
    :return:
    """
    test_result = expected_value == test_value

    if not test_result:
        print(expected_value, "!=", test_value)
        print(type(expected_value))
        print(type(test_value))

    return test_result


class ParserTests(unittest.TestCase):

    def test__find_numbers_of_ref_and_alt_reads_t1(self):
        self.assertTrue(find_numbers_of_ref_and_alt_reads("1/1:0,2:2:6:65,6,0") == (0, 2))

    def test__find_numbers_of_ref_and_alt_reads_t2(self):
        input_string = "ABHom=1.00;AC=2;AF=1.00;BaseCounts=BaseCounts=0,0,2,0;ExcessHet=3.0103;"
        self.assertTrue(find_numbers_of_ref_and_alt_reads(input_string) == (0, 2))

    def test__parse_gtf_file(self):
        test_gtf_path = test_path + "parsers/test.gtf"
        gff_obj = GFF(test_gtf_path)
        # 1	ensembl_havana	gene	65419	71585	.	+	.

        expected_value = []
        test_value = ["1", 65419, 71585, "+"]
        for record in gff_obj.yield_lines():
            expected_value = [record.chromosome, record.start, record.end, record.strand]
            break

        self.assertTrue(test_values(expected_value, test_value))

    def test__GenomicIntervalTree__get_features_overlapping_position(self):

        test_gtf_path = test_path + "parsers/test.gtf"

        interval_tree = GenomicIntervalTree(test_gtf_path)
        # Should return intergenic
        features = interval_tree.get_features_overlapping_position("1", 10, "+")
        expected_value = ['intergenic']
        self.assertTrue(test_values(expected_value, features))

        features = set(interval_tree.get_features_overlapping_position("1", 65429, "+"))
        expected_value = {'exon', 'five_prime_utr'}

        self.assertTrue(test_values(expected_value, features))


    def test__FASTA__find_countable_character_positions_1(self):
        # fetch records in a region using 0-based indexing
        sample_fasta = """>test_chromosome_1\nACGTACGTACGT\n"""
        expected_value = {0, 4, 8}

        temp_file = NamedTemporaryFile(mode='w', prefix='tmp')
        temp_file.write(sample_fasta)
        temp_file.flush()

        test_fasta = FASTA(temp_file.name)
        test_value = test_fasta.find_countable_character_positions("test_chromosome_1", 0, 12, "A")

        temp_file.close()
        #os.unlink(temp_file.name)

        self.assertTrue(test_values(expected_value, test_value))


    def test__FASTA__find_countable_character_positions_2(self):
        # fetch records in a region using 0-based indexing
        sample_fasta = """>test_chromosome_1\nACGTACGTACGT\n"""
        expected_value = {3, 7, 11}

        temp_file = NamedTemporaryFile(mode='w', prefix='tmp')
        temp_file.write(sample_fasta)
        temp_file.flush()

        test_fasta = FASTA(temp_file.name)
        test_value = test_fasta.find_countable_character_positions("test_chromosome_1", 0, 12, "T")

        temp_file.close()

        self.assertTrue(test_values(expected_value, test_value))


class SharedTests(unittest.TestCase):

    def test__impute_strand__find_pos_strand(self):
        """ Should return true if greater than or equal to min_coverage.
        """
        a_to_g_cnt = 15
        t_to_c_cnt = 2

        # A to G conversions indicate the positive strand.
        expected_value = "+"
        test_value = impute_strand(a_to_g_cnt, t_to_c_cnt)

        self.assertTrue(test_values(expected_value, test_value))

    def test__impute_strand__find_neg_strand(self):
        """ Should return true if greater than or equal to min_coverage.
        """
        a_to_g_cnt = 2
        t_to_c_cnt = 12

        # A to G conversions indicate the positive strand.
        expected_value = "-"
        test_value = impute_strand(a_to_g_cnt, t_to_c_cnt)

        self.assertTrue(test_values(expected_value, test_value))

    def test__impute_strand__cannot_decide(self):
        """ Should return true if greater than or equal to min_coverage.
        """
        a_to_g_cnt = 12
        t_to_c_cnt = 12

        # A to G conversions indicate the positive strand.
        expected_value = "."
        test_value = impute_strand(a_to_g_cnt, t_to_c_cnt)

        self.assertTrue(test_values(expected_value, test_value))

    def test__passes_min_coverage__1(self):
        """ Should return true if greater than or equal to min_coverage.
        """
        coverage = 5
        min_coverage = 5
        self.assertTrue(passes_min_coverage(coverage, min_coverage))

    def test__passes_min_coverage__2(self):
        """ Should return true if greater than or equal to min_coverage.
        """
        coverage = 10
        min_coverage = 5
        self.assertTrue(passes_min_coverage(coverage, min_coverage))

    def test__passes_min_coverage__3(self):
        """ Should return true if greater than or equal to min_coverage.
        """
        coverage = 2
        min_coverage = 5
        self.assertFalse(passes_min_coverage(coverage, min_coverage))

    """ Should return true if greater than or equal to min_editing.
    """
    def test__passes_min_editing__1(self):
        editing = 5
        min_editing = 5
        self.assertTrue(passes_min_coverage(editing, min_editing))

    def test__passes_min_editing__2(self):
        """ Should return true if greater than or equal to min_coverage.
        """
        editing = 10
        min_editing = 5
        self.assertTrue(passes_min_coverage(editing, min_editing))

    def test__passes_min_editing__3(self):
        """ Should return true if greater than or equal to min_coverage.
        """
        editing = 2
        min_editing = 5
        self.assertFalse(passes_min_coverage(editing, min_editing))

    def test__choose__1(self):
        self.assertTrue(choose(10, 5) == 252)

    def test__determine_aggregation_depth__1(self):
        self.assertTrue(determine_aggregation_depth(2) == 45)

    def test__get_strand__1(self):
        record_strand = "+"
        a_to_g_cnt = 0
        c_to_t_cnt = 0
        self.assertTrue(get_strand(record_strand, a_to_g_cnt, c_to_t_cnt) == "A")

    def test__get_strand__2(self):
        record_strand = "-"
        a_to_g_cnt = 0
        c_to_t_cnt = 0
        # test_values(expected_value, test_value)
        expected_value = "T"
        test_value = get_strand(record_strand, a_to_g_cnt, c_to_t_cnt)
        self.assertTrue(test_values(expected_value, test_value))

    def test__get_strand__3(self):
        record_strand = "."
        a_to_g_cnt = 10
        c_to_t_cnt = 1
        self.assertTrue(get_strand(record_strand, a_to_g_cnt, c_to_t_cnt) == "A")

    def test__get_strand__4(self):
        record_strand = "."
        a_to_g_cnt = 2
        c_to_t_cnt = 7
        expected_value = "T"
        test_value = get_strand(record_strand, a_to_g_cnt, c_to_t_cnt)

        self.assertTrue(test_values(expected_value, test_value))

    def test__base_transition_tuples_and_titles__4(self):

        transition_tuple_titles, transition_tuple_list = base_transition_tuples_and_titles()

        transition_tuple_titles_test = [
            'A>C', 'A>G', 'A>T', 'C>A', 'C>G', 'C>T', 'G>A', 'G>C', 'G>T', 'T>A', 'T>C', 'T>G'
        ]
        transition_tuple_list_test = [
            ('A', 'C'), ('A', 'G'), ('A', 'T'), ('C', 'A'), ('C', 'G'), ('C', 'T'),
            ('G', 'A'), ('G', 'C'), ('G', 'T'), ('T', 'A'), ('T', 'C'), ('T', 'G')
        ]

        self.assertTrue(transition_tuple_titles == transition_tuple_titles_test)
        self.assertTrue(transition_tuple_list == transition_tuple_list_test)


class MergeTests(unittest.TestCase):

    def test__passes_min_coverage__1(self):
        """ Should return true if greater than or equal to min_coverage.
        """
        coverage = 5
        min_coverage = 5
        self.assertTrue(passes_min_coverage(coverage, min_coverage))

if __name__ == '__main__':
    unittest.main()


