# DRETools

DRETools is a command-line tools set for finding differential RNA editing and realted fuctions.
See the DRETools homepage at https://dretools.bitbucket.io/ for more information. 


### Installation

Installing DRETools requires two steps. 

1) Installing python packages with pip. This can be done with the command "pip3 install dretools". This will handle all python-based dependencies. 

2) Additionally R version 3.4.1 or greater is needed.   


### Usage

To view functions available in DRETools simply type "dretools --help". 
