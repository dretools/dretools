from behave import *
import shlex, subprocess
import sys


def show_diffs(command, captured_output, expected_output):
    print("command ------------------------------------------------------------")
    print(command)

    print("captured_output ----------------------------------------------------")
    print(captured_output)

    print("expected_output ----------------------------------------------------")
    print(expected_output)


@given("argument {} with the value {}")
def step_impl(context, argument, value):
    """
    :param context: behave.runner.Context
    :param argument:
    :param value:
    :return:
    """

    sys.argv.append(argument)
    sys.argv.append(value)


@given("the additional argument {}")
def step_impl(context, value):
    """
    :param context: behave.runner.Context
    :param value:
    :return:
    """
    sys.argv.append(value)


@when('we run the operation {} from dretools')
def step_impl(context, function):
    """
    :type context: behave.runner.Context
    :param function:
    :return:
    """
    cli_args = ["./dretools", function] + sys.argv[1:]
    context.cli_command = " ".join(cli_args)
    run_command = subprocess.run(cli_args, stdout=subprocess.PIPE)
    out_txt = run_command.stdout.decode('ascii').strip()
    # The STDOUT is captured by behave in the variable context.stdout_capture
    print(out_txt)


@then('we expect the output')
def step_impl(context):
    """
    :type context: behave.runner.Context
    """
    expected_output = context.text.strip()
    captured_output = context.stdout_capture.getvalue().strip()
    assert captured_output == expected_output, show_diffs(context.cli_command, captured_output, expected_output)

