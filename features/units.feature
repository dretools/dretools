
Feature: Test dretools


Scenario: Test basic edsite-merge usage.
    Given argument --min-editing with the value 3
    Given argument --min-coverage with the value 5
    Given argument --min-samples with the value 3
    Given argument --vcf with the value examples/VCF/SRR3091828.ex.vcf
    Given the additional argument examples/VCF/SRR3091829.ex.vcf
    Given the additional argument examples/VCF/SRR3091830.ex.vcf
    Given the additional argument examples/VCF/SRR3091831.ex.vcf
    Given the additional argument examples/VCF/SRR3091832.ex.vcf
    Given the additional argument examples/VCF/SRR3091833.ex.vcf
    When we run the operation edsite-merge from dretools
    Then we expect the output
    """
    #Chromosome	Position	ID	Ref	Alt	Qual	Fil	Sample_cnt
    1	20649529	.	T	C	.	.	3
    1	20649581	.	T	C	.	.	4
    1	20649584	.	T	C	.	.	4
    17	59201902	.	A	G	.	.	4
    17	59201920	.	A	G	.	.	3
    17	59203697	.	A	G	.	.	3
    19	3647729	.	T	C	.	.	4
    19	3647846	.	T	C	.	.	3
    19	3647939	.	T	C	.	.	4
    19	3648162	.	T	C	.	.	6
    19	3648201	.	T	C	.	.	5
    19	3648204	.	T	C	.	.	4
    19	3648221	.	T	C	.	.	5
    19	3648300	.	T	C	.	.	4
    19	56379263	.	A	G	.	.	6
    19	56379264	.	A	G	.	.	6
    19	56379333	.	A	G	.	.	3
    2	128192922	.	A	G	.	.	6
    2	128192923	.	A	G	.	.	4
    2	128192967	.	A	G	.	.	4
    2	128192973	.	A	G	.	.	3
    2	128193417	.	A	G	.	.	3
    3	40537447	.	A	G	.	.	3
    4	17801265	.	T	C	.	.	3
    4	17801355	.	T	C	.	.	4
    4	17801913	.	T	C	.	.	5
    4	17801935	.	T	C	.	.	4
    6	149724711	.	T	C	.	.	4
    6	149724723	.	T	C	.	.	5
    6	149725408	.	T	C	.	.	4
    6	149725422	.	T	C	.	.	6
    6	149725536	.	T	C	.	.	3
    7	38723473	.	T	C	.	.	4
    7	38723550	.	T	C	.	.	5
    7	38723599	.	T	C	.	.	5
    7	38723601	.	T	C	.	.	3
    7	38723611	.	T	C	.	.	3
    """


Scenario: Test basic sample-epk usage.
    Given argument --name with the value SRR3091828
    Given argument --vcf with the value examples/consensus_sites.vcf
    Given argument --alignment with the value examples/BAM/SRR3091828.ex.bam
    When we run the operation sample-epk from dretools
    Then We expect the output
    """
    #Sample_Name	Editable_Area	Average_Depth	Total_Ref_Bases	Total_Alt_Bases	EPK
    SRR3091828	34	8	140	120	857.14286
    """


Scenario: Test basic edsite-epk usage.
    Given argument --vcf with the value examples/consensus_sites.vcf
    Given argument --alignment with the value examples/BAM/SRR3091828.ex.bam
    When we run the operation edsite-epk from dretools
    Then We expect the output
    """
    #Site_Location	Area	Depth	Ref_Bases	Alt_Bases	EPK
    19:56379264	1	9	3	6	2000.0
    6:149725422	1	8	2	6	3000.0
    3:40537447	0	0	0	0	0
    19:3648221	0	0	0	0	0
    6:149725408	1	9	6	3	500.0
    2:128192967	1	6	4	2	500.0
    1:20649584	0	0	0	0	0
    19:56379263	1	9	2	7	3500.0
    4:17801935	1	7	1	6	6000.0
    2:128193417	1	4	1	3	3000.0
    19:3647729	1	3	2	1	500.0
    4:17801913	1	6	3	3	1000.0
    19:56379333	1	10	7	3	428.57143
    19:3648204	1	5	4	1	250.0
    6:149724711	1	7	3	4	1333.33333
    2:128192923	1	9	8	1	125.0
    7:38723473	1	10	7	3	428.57143
    4:17801265	1	8	5	3	600.0
    2:128192973	1	5	3	2	666.66667
    4:17801355	1	13	9	4	444.44444
    6:149725536	1	6	3	3	1000.0
    1:20649581	1	1	1	0	0.0
    7:38723611	1	11	7	4	571.42857
    17:59203697	1	11	9	2	222.22222
    19:3648162	1	6	1	5	5000.0
    2:128192922	1	9	5	4	800.0
    19:3647939	1	8	2	6	3000.0
    7:38723550	1	8	4	4	1000.0
    19:3648300	1	4	3	1	333.33333
    19:3647846	1	8	1	7	7000.0
    7:38723601	1	11	7	4	571.42857
    7:38723599	1	11	9	2	222.22222
    19:3648201	1	5	3	2	666.66667
    1:20649529	1	5	3	2	666.66667
    17:59201920	1	10	4	6	1500.0
    17:59201902	1	11	4	7	1750.0
    6:149724723	1	7	4	3	750.0
    """


Scenario: Test basic edsite-epk usage.
    Given argument --max-depth-cov with the value 5.0
    Given argument --min-depth with the value 2
    Given argument --names with the value scrRNA,siRNA
    Given argument --sites with the value examples/consensus_sites.vcf
    Given argument --sample-epk with the value examples/EPK/SRR3091828.sample_epk.tsv,examples/EPK/SRR3091829.sample_epk.tsv,examples/EPK/SRR3091830.sample_epk.tsv
    Given the additional argument examples/EPK/SRR3091831.sample_epk.tsv,examples/EPK/SRR3091832.sample_epk.tsv,examples/EPK/SRR3091833.sample_epk.tsv
    Given argument --site-epk with the value examples/EPK/SRR3091828.edsite_epk.tsv,examples/EPK/SRR3091829.edsite_epk.tsv,examples/EPK/SRR3091830.edsite_epk.tsv
    Given the additional argument examples/EPK/SRR3091831.edsite_epk.tsv,examples/EPK/SRR3091832.edsite_epk.tsv,examples/EPK/SRR3091833.edsite_epk.tsv
    When we run the operation edsite-diff from dretools
    Then We expect the output
    """
    #Group_1	Group_2	Record_Name	Group_1_Mean	Group_2_Mean	LM_pvalue	ttest_pvalue
    scrRNA	siRNA	19:56379264	2055.56	469.44	0.4762386	0.0017602
    scrRNA	siRNA	6:149725422	2585.86	609.26	0.2510187	0.0659989
    scrRNA	siRNA	6:149725408	555.56	185.61	0.901597	0.0050499
    scrRNA	siRNA	2:128192967	1654.76	191.9	0.1133313	0.0891759
    scrRNA	siRNA	19:56379263	3944.44	1240.69	0.5675211	0.026205
    scrRNA	siRNA	4:17801935	5611.11	280.42	0.1914355	0.062788
    scrRNA	siRNA	19:3647729	1000.0	647.62	0.2024545	0.487569
    scrRNA	siRNA	19:56379333	578.04	83.33	0.2908171	0.0092672
    scrRNA	siRNA	19:3648204	837.3	542.09	0.0822975	0.4873429
    scrRNA	siRNA	6:149724711	1311.11	226.19	0.4385503	0.0623075
    scrRNA	siRNA	2:128192923	448.61	168.31	0.4537666	0.1761602
    scrRNA	siRNA	7:38723473	1952.38	117.79	0.4906308	0.160222
    scrRNA	siRNA	4:17801265	409.52	205.87	0.1152192	0.2207004
    scrRNA	siRNA	2:128192973	947.09	160.14	0.4541501	0.0124649
    scrRNA	siRNA	4:17801355	693.6	166.31	0.8548656	0.0376054
    scrRNA	siRNA	6:149725536	777.78	78.7	0.1801114	0.0040598
    scrRNA	siRNA	7:38723611	1540.48	95.78	0.5072169	0.1672943
    scrRNA	siRNA	17:59203697	424.07	143.98	0.3820313	0.2318588
    scrRNA	siRNA	19:3648162	5888.89	988.89	0.6865653	0.2124562
    scrRNA	siRNA	2:128192922	1647.62	393.89	0.666986	0.0460509
    scrRNA	siRNA	19:3647939	1254.55	133.57	0.0255928	0.2685192
    scrRNA	siRNA	7:38723550	1428.57	405.3	0.0457155	0.0389822
    scrRNA	siRNA	19:3648300	420.63	527.78	0.5577789	0.7902648
    scrRNA	siRNA	19:3647846	3027.78	966.67	0.760773	0.3961742
    scrRNA	siRNA	7:38723601	1317.46	55.56	0.8609446	0.0287326
    scrRNA	siRNA	7:38723599	496.3	286.74	0.0123799	0.2388779
    scrRNA	siRNA	19:3648201	875.0	711.11	0.5512829	0.7141435
    scrRNA	siRNA	1:20649529	1361.11	260.61	0.2520423	0.1793394
    scrRNA	siRNA	17:59201920	1484.13	48.88	0.0466401	0.0002033
    scrRNA	siRNA	17:59201902	1983.33	179.24	0.7545107	0.0138202
    scrRNA	siRNA	6:149724723	916.67	511.11	0.049054	0.1137977
    """


Scenario: Test basic sample-epk usage.
    Given argument --min-editing with the value 3
    Given argument --min-coverage with the value 5
    Given argument --min-length with the value 20
    Given argument --min-points with the value 5
    Given argument --epsilon with the value 50
    Given argument --vcf with the value examples/VCF/SRR3091828.ex.vcf
    Given the additional argument examples/VCF/SRR3091829.ex.vcf
    Given the additional argument examples/VCF/SRR3091830.ex.vcf
    Given the additional argument examples/VCF/SRR3091831.ex.vcf
    Given the additional argument examples/VCF/SRR3091832.ex.vcf
    Given the additional argument examples/VCF/SRR3091833.ex.vcf
    When we run the operation find-islands from dretools
    Then We expect the output
    """
    #Chromosome	Start	End	ID	Score	Strand	Length	Number_of_Sites	Density
    2	128192920	128193033	ub5Kpx5pky615pmsdq4PiQ	.	+	113	7	0.06195
    19	3648149	3648239	SUz1q7g3dUYHVjlhFK4ZAg	.	-	90	8	0.08889
    4	17801262	17801389	Z1kllXDbos-rNv9UTjIXzA	.	-	127	6	0.04724
    6	149725405	149725553	kt31w8egfUS3NGIlr1dTeg	.	-	148	10	0.06757
    7	38723547	38723651	kluKgmv4-KKrQsRMYX_iCA	.	-	104	6	0.05769
    """


Scenario: Test basic edsite-epk usage.
    Given argument --regions with the value examples/OUT/islands.bed
    Given argument --min-area with the value 1
    Given argument --min-depth with the value 1
    Given argument --names with the value scrRNA,siRNA
    Given argument --sample-epk with the value examples/EPK/SRR3091828.sample_epk.tsv,examples/EPK/SRR3091829.sample_epk.tsv,examples/EPK/SRR3091830.sample_epk.tsv
    Given the additional argument examples/EPK/SRR3091831.sample_epk.tsv,examples/EPK/SRR3091832.sample_epk.tsv,examples/EPK/SRR3091833.sample_epk.tsv
    Given argument --region-epk with the value examples/EPK/SRR3091828.island_epk.tsv,examples/EPK/SRR3091829.island_epk.tsv,examples/EPK/SRR3091830.island_epk.tsv
    Given the additional argument examples/EPK/SRR3091831.island_epk.tsv,examples/EPK/SRR3091832.island_epk.tsv,examples/EPK/SRR3091833.island_epk.tsv
    When we run the operation region-diff from dretools
    Then We expect the output
    """
    #Group_1	Group_2	Record_Name	Group_1_Mean	Group_2_Mean	LM_pvalue	ttest_pvalue
    scrRNA	siRNA	kt31w8egfUS3NGIlr1dTeg	6:149725405-149725553	997.62	281.94	0.121698	0.0067977
    scrRNA	siRNA	kluKgmv4-KKrQsRMYX_iCA	7:38723547-38723651	1029.32	180.94	0.330851	0.0479502
    scrRNA	siRNA	Z1kllXDbos-rNv9UTjIXzA	4:17801262-17801389	559.52	171.33	0.3343707	0.0207269
    scrRNA	siRNA	SUz1q7g3dUYHVjlhFK4ZAg	19:3648149-3648239	1579.41	913.73	0.7009463	0.3207373
    scrRNA	siRNA	ub5Kpx5pky615pmsdq4PiQ	2:128192920-128193033	982.72	219.28	0.9763355	0.0474272
    """

# @TODO: Make sure files shown in text match those used



